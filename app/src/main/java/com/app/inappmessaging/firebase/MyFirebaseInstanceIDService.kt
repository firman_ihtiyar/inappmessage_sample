package com.app.inappmessaging.firebase

import android.util.Log
import com.google.firebase.iid.InstanceIdResult
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

public class MyFirebaseInstanceIDService : FirebaseMessagingService() {
    val a = "MyFirebaseMessaging" as String
    override fun onNewToken(s: String?) {
        super.onNewToken(s)
        Log.i(a,"TOKEN " + s)
    }


    override fun onMessageReceived(p0: RemoteMessage?) {
        super.onMessageReceived(p0)
        Log.i( a, "MESSAGE "+ p0?.notification?.body)
    }

}
